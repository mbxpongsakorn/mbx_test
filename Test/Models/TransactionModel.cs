﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Models
{
	public class TransactionModel
	{
		public int id { get; set; }
		public string date { get; set; }
		public double amount { get; set; }
		public string currency { get; set; }
		public string status { get; set; }
	}
}
