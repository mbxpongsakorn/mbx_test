﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Models
{
	public class SearchCustomer
	{

		//[MaxLength(10, ErrorMessage = "Invalid Customer ID")]
		public int customerID { get; set; }
		[EmailAddress(ErrorMessage = "Invalid Email")]
		[StringLength(25, ErrorMessage = "Invalid Email")]
		public string email { get; set; }
	}
}
