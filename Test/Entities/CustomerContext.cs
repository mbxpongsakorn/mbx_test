﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Entities
{
	public class CustomerContext : DbContext
	{
		public CustomerContext(DbContextOptions options)
			: base(options)
		{
		}

		public DbSet<Customer> Customers { get; set; }
		public DbSet<Transaction> Transactions { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Transaction>()
			.HasOne<Customer>(s => s.Customer)
			.WithMany(g => g.Transactions)
			.HasForeignKey(s => s.customerID);
		}
	}
}
