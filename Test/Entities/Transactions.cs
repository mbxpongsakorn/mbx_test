﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Test.Entities
{
	[Table("Transactions")]
	public class Transaction
	{
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int ID { get; set; }
		public DateTime Date { get; set; }
		public double Amount { get; set; }
		public string Currency { get; set; }
		public string Status { get; set; }
		public int customerID { get; set; }
		public Customer Customer { get; set; }
	}
}
