﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Test.Entities;
using Test.Models;
using Test.Repositorys;
using Customer = Test.Entities.Customer;

namespace Test.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
		private readonly IRepository<Customer> _customerRepo;
		public CustomerController(IRepository<Customer> customerRepo)
		{
			this._customerRepo = customerRepo;
		}
		// GET api/customer
		[HttpGet]
		public string GetAll(int id)
		{
			return "api ok";
		}

		// GET api/values/5
		[HttpPost]
		public IActionResult Get(SearchCustomer search)
		{
			try
			{
				if (!ModelState.IsValid) return BadRequest(ModelState);
				if (search == null) return BadRequest("No inquiry criteria");

				Customer customer;
				customer = _customerRepo.Get(search.customerID, search.email);
				if (customer == null) return NotFound("Not found");

				CustomerModel result = new CustomerModel();
				result.customerID = customer.CustomerID;
				result.name = customer.Name;
				result.email = customer.Email;
				result.mobile = customer.Mobile;
				result.transactions = customer.Transactions.Select(x => new TransactionModel
				{
					id = x.ID,
					date = x.Date.ToString("dd/MM/yyyy HH:mm"),
					amount = x.Amount,
					currency =x.Currency,
					status = x.Status
				}).ToList();

				return Ok(result);
			}
			catch(Exception ex)
			{
				return BadRequest();
			}
		}
	}
}