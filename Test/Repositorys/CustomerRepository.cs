﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Test.Entities;

namespace Test.Repositorys
{
	public class CustomerRepository : IRepository<Customer>
	{
		private readonly CustomerContext _context;
		public CustomerRepository(CustomerContext context)
		{
			_context = context;
		}

		public Customer Get(int id, string email)
		{
			return _context.Customers.Where(x => x.CustomerID == id || x.Email == email)
				.Select(x => new Customer
				{
					CustomerID = x.CustomerID,
					Email = x.Email,
					Name = x.Name,
					Mobile = x.Mobile,
					Transactions = x.Transactions.Where(b => b.customerID == x.CustomerID).ToList()
				}).FirstOrDefault();
		}

		Customer IRepository<Customer>.Get(int id)
		{
			return _context.Customers.Where(x => x.CustomerID == id)
				.Select(x => new Customer
				{
					CustomerID = x.CustomerID,
					Email = x.Email,
					Name = x.Name,
					Mobile = x.Mobile,
					Transactions = x.Transactions.Where(b => b.customerID == id).ToList()
				}).FirstOrDefault();
		}
	}
}
