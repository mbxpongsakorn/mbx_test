CREATE TABLE Customers (
    customerID INT NOT NULL PRIMARY KEY,
    name VARCHAR(30),
    email VARCHAR(25),
    mobile VARCHAR(10),
);

CREATE TABLE Transactions (
    id INT NOT NULL PRIMARY KEY,
    date DATETIME2,
    amount FLOAT,
    currency VARCHAR(3),
	status VARCHAR(10),
	customerID INT FOREIGN KEY REFERENCES Customers(customerID)
);

INSERT INTO Customers (customerID, name, email, mobile) VALUES (123456, 'Firstname Lastname', 'user@domain.com', '0123456789')
INSERT INTO Transactions (id, customerID, date, amount, currency, status) VALUES (1, 123456, GETDATE(), 1234.56, 'USD', 'Success')
INSERT INTO Transactions (id, customerID, date, amount, currency, status) VALUES (2, 123456, GETDATE(), 1234.56, 'THB', 'Failed')
INSERT INTO Transactions (id, customerID, date, amount, currency, status) VALUES (3, 123456, GETDATE(), 1234.56, 'JPY', 'Success')
INSERT INTO Transactions (id, customerID, date, amount, currency, status) VALUES (4, 123456, GETDATE(), 1234.56, 'USD', 'Failed')
INSERT INTO Transactions (id, customerID, date, amount, currency, status) VALUES (5, 123456, GETDATE(), 1234.56, 'SGD', 'Success')